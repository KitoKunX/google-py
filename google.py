import requests
import json
import sys

#some constant for cleaning dictionary
non_bmp_map = dict.fromkeys(range(0x10000, sys.maxunicode + 1), 0xfffd)


class Google:

    def __init__(self, key, sid = ""):
        self.apiKey = key
        self.customSearchURL = "https://www.googleapis.com/customsearch/v1"
        self.searchEngineID = sid
        self.safeLevel = "medium"

    #change api token
    def setKey(self, key):
        self.apiKey = arg

    #make a search engine app on google console
    def setSearchEngineID(self, id):
        self.searchEngineID = id

    #function to set safe search levels (off, medium, high)
    def setSafeLevel(self, level):
        self.safeLevel = level

    #search results but with the raw dictionary
    def _rawSearch(self, query, amount = 5):
        payload = {"q":query,
                   "key":self.apiKey,
                   "num":amount,
                   "cx":self.searchEngineID,
                   "safe":self.safeLevel
                   }
        req = requests.get(self.customSearchURL, params = payload)
        return json.loads(req.text.translate(non_bmp_map))

    #google custom search function (returns only the links)
    def customSearch(self, query, amount = 5):
        rawData = self._rawSearch(query, amount)
        return [i["link"] for i in rawData["items"]]

    #image search function, too lazy, will finish later
    def imageSearch(self, query):
        return None
    
#example usage
if __name__ == "__main__":
    google = Google("API KEY HERE")
    google.setSearchEngineID("SEARCH ENGINE ID HERE")
    print(google.customSearch("Your Search Query Here"))
        